![GitLabl Build Status](https://git.obarun.org/Obarun/66-tools/badges/master/pipeline.svg)

66-tools - Helpers tools to accomplish various and repetitive tasks in service scripts administration common tasks
====

Some utilities are language [execline](https://skarnet.org/software/execline) specific (usually named with `execl-` prefix) where other can be used on classic shell.

## Installation

See the INSTALL.md file for 66 upstream compile/build notes.

## Recommended build instructions

The recommended method to build this package directly from git.
```
gbp clone https://gitlab.com/init-diversity/s6-66/66-tools.git && 
cd 66-tools && 
gbp buildpackage -uc -us
```

The following should get you all the software required to build using this method:

`sudo apt install git-buildpackage s6 oblibs skalibs-dev libs6-dev libexecline-dev execline libexecline2.9 libskarnet2.14 lowdown`


Documentation
-------------

Online [documentation](https://web.obarun.org/software/66-tools/)

Contact information
-------------------

* Email:
  Eric Vidal `<eric@obarun.org>`

* Mailing list
  https://obarun.org/mailman/listinfo/66_obarun.org/

* Web site:
  https://web.obarun.org/

* XMPP Channel:
  obarun@xmpp.obarun.org

Supports the project
---------------------

Please consider to make [donation](https://web.obarun.org/index.php?id=18)
